from textwrap import dedent
from sys import exit
from ex45_API import Room
from ex45_API import One
from ex45_API import Two
from ex45_API import Three
from ex45_API import Four
from ex45_API import Five
from ex45_API import Six
from ex45_API import Seven
from ex45_API import Eight
from ex45_API import Nine
from ex45_API import Exit
from ex45_API import GetObject
from ex45_Responses import UseObjectResponses
from ex45_API import UseObject


class Commands(object):
    '''
    Commands class. This class will used the command word passed into here to
    retrieve the corresponding function call.

    help(), inventory(), look()

    Attributes:
        room (str): The current room the player is in.
        command (str): The command to be executed.
    '''

    def __init__(self, room, words):
        '''
        Constructor for Commands class.

        Attributes:
            room (str): The current room the player is in.
            words (str): The remainder of the string from the original player
                         entered command (if any exists). Needed for the "use"
                         and "get" commands to use or get something.
        '''

        self.room = room
        self.words = words

    def help(self):
        '''
        Function to provide help to the player.
        '''
        print(dedent("""

                 You have several commands at your disposal:

                 help - This command

                 inventory - List everything you have acquired

                 look - Look around a room (in case your memory sucks and
                        you forgot what was in there)

                 get [item] - If the item is a multiple word thing you would
                              enter all the words. For example if you wanted to
                              get a box of apples you would type 'get box of
                              apples' rather than 'get box' or 'get apples'.

                              If the object uses quotes you would include them.
                              For example, if you wanted to get some "Golden
                              Delicious" apples you would type 'get "Golden
                              Delicious" apples'.

                 use [item] - Use an item in your inventory. The item name should
                              be typed verbatim to how it's spelled and punctuated
                              in your inventory.

                              'use [item] with [item]' (where applicable)

                 taunt - Taunt something

                 Type a door name to leave a room (e.g "Two", "Four", etc...)

                 Remember to follow capitalization and punctuation as shown.

                 If you get stuck you can always cheat and look at the code loser...
                 """))

    def inventory(self):
        '''
        Function to inventory the items the player is currently carrying.
        '''
        print("\nThis is what you are carrying:")
        print(*GetObject.pouch, sep = '\n')

    def look(self, room):
        '''
        Function to look around a room.

        Parameters:
            room (str): The current room you are in.
        '''
        # Retrieves room class based on the current room value
        next_room = Map.scenes[self.room.upper()]

        # Sets room_state to whatever values exist in the Map.room_state[]
        # dictionary for the room the player is entering. Each value is
        # the same string value used in the GetObject.object_status[]
        # dictionary.
        room_state = Map.room_state[self.room.upper()]





        # Room FIVE is a special case because you have to get rid of
        # the pirate before you can grab the can of honey. This if
        # starts by checking to see if the pirate is gone from the room.
        if GetObject.object_status[room_state[0]] == 1 and self.room.lower() == "five":

            # if the can has already been gotten by the player
            if GetObject.object_status[room_state[1]] == 1:
                next_room_key = next_room.look_empty_room()
            else:
                next_room_key = next_room.look_can()

        # Room NINE has a special use case because there are two bear scenarios;
        # one for if the bear is eating and one for if he's not eating. There
        # is no empty room scenario in room NINE
        elif GetObject.object_status[room_state[0]] == 1 and self.room.lower() == "nine":
            next_room_key = next_room.look_bear_eating()

        else:
            # All other rooms have only one entry and one re-entry scenario so this
            # if statement covers all of them. If the room's object has been
            # grabbed the room will be empty and look empty.
            if GetObject.object_status[room_state[0]] == 1:
                next_room_key = next_room.look_empty_room()
            else:
                next_room_key = next_room.look()


        # Retrieves the available doors to use based on the current room
        doors = Room.maze[self.room.upper()]
        door_list = ', '.join(doors)
        print("You see the following doors leading out of the room:", door_list)

    def taunt(self, room):
        '''
        Function to taunt something. Taunting is only allowed in rooms FIVE
        and NINE. In FIVE it only works if the pirate is present.

        Parameters:
            room (str): The current room you are in.
        '''
        # Retrieves room class based on the current room value
        next_room = Map.scenes[self.room.upper()]

        # The following set of if statements cover what the look command
        # will reveal. It will depend on which room the player is in and the
        # status of any items that originated in that room.
        if self.room.lower() == "five" and GetObject.object_status['pirate_gone'] == 0:
                next_room_key = next_room.taunt()
        elif self.room.lower() == "nine":
            if GetObject.object_status['bear_eating'] == 0:
                next_room_key = next_room.taunt()
            else:
                next_room_key = next_room.taunt_eating()
        else:
            print(dedent("""
                         Taunt what? There's nothing to taunt here."""))

    def get(self, room, words):
        '''
        Function to call the GetObject() class and pass the parameters brought
        into this function on to it.

        Parameters:
            room  (Room): The current room string.
            words (list): List of strings broken up from the command entered
            in Commands()
        '''

        function_pass_through = GetObject(room, words)
        function_pass_through.grab(room, words)

    def use(self, room, words):
        '''
        Function to call the UseObject() class and pass the parameters brought
        into this function on to it.

        Parameters:
            room  (Room): The current room string.
            words (list): List of strings broken up from the command entered
            in Commands()
        '''

        function_pass_through = UseObject(room, words)
        function_pass_through.using(room, words)

    # Dictionary command_dict stores the functions in the Command() class.
    # They are accessed by Engine.prompt() to match the player's command to
    # the corresponding function in Commands().
    command_dict = {
                   "HELP": help,
                   "LOOK": look,
                   "INVENTORY": inventory,
                   "TAUNT": taunt,
                   "GET": get,
                   "USE": use
    }

class Engine(object):
    '''
    Engine class. Contains functions pertaining to text input in the game.

    Attributes:
        room (str): The current room the player is in.
        doors (list): List of available doors based on the current room.
    '''
    def __init__(self, room, doors):
        '''
        Constructor for Engine class.

        Attributes:
            room (str): The current room the player is in.
            doors (list): List of available doors based on the current room.
        '''
        self.room = room
        self.doors = doors

    def prompt(self):
        '''
        Engine.prompt() is the function for player text input. Engine.prompt()
        will loop forever until an expected word (or words) are entered. If
        nothing recognizeable is entered the player will be prompted to enter
        text again.

        If the word entered is a door name, it will be returned as a string to
        Map.play().

        If it is a single word command (e.g. 'Help', etc.), functions in other
        classes will be called to carry out the command. The functions called
        will depend on the command entered and the room the player is in. The
        code will eventually return to this Engine.prompt() where it will
        continue to loop.

        If it is a sentence (multiple word commands), further parsing to the
        string will occur to figure out what command is being given. Based on
        that, functions in other classes will be called to carry out the
        command. The code will eventually return to this Engine.prompt() where
        it will continue to loop.

        Attributes:
            room (str): The current room the player is in.
            doors (list): List of available doors based on the current room.

        Returns:
            self.room: The string containing the door the player chooses to go
            through will be returned to Map.play()
        '''

        while self.room:
            print("\nWhat do you want to do?")
            command = str(input("> "))

            # we split up the string into a list of words here
            # this is so we can search the list for keywords
            # when the code needs to figure out what multi word
            # command has been entered
            words = command.split(' ')

            # If command is a door name that exists in that room.
            if command in self.doors:

                # Change the current room name to the door chosen and return
                # it back to Map.play()
                self.room = command
                return self.room

            # Since command not an available door name it may be one of several
            # valid player commands so we pop it off words to check it.
            command_word = words.pop(0)

            # If the player entered command is to do something that is a valid
            # command, then execute the corresponding function in Commands()
            # for the command given. The nested if statements exist so that
            # the function to be called will have passed the number of
            # parameters on to it.
            if command_word.upper() in Commands.command_dict.keys():
                function_to_call = Commands.command_dict[command_word.upper()]
                if command_word.upper() in ['HELP', 'INVENTORY']:
                    function_to_call(self)
                elif command_word.upper() in ['LOOK', 'TAUNT']:
                    function_to_call(self, self.room)
                elif command_word.upper() in ['GET', 'USE']:
                    function_to_call(self, self.room, words)
                else:
                    print("YOU SCREWED UP THE CODE!!!")

            # If you entered a door name wrong or entered an invalid command
            # or made a typo, the code will ask you to re-enter the command and
            # the loop will start over again.
            else:
                print("\nI didn\'t understand that. Try again.")

class Map(object):
    '''
    Map class. Contains functions pertaining to moving from room to room.

    Attributes:
        room (str): The current room the player is in.
    '''
    # Dictionay that holds the room classes. This exists so that Map.play()
    # won't need 10 nested if statements to check and see which room class to
    # use based on the current room value.
    scenes = {
              'ONE': One(Room),
              'TWO': Two(Room),
              'THREE': Three(Room),
              'FOUR': Four(Room),
              'FIVE': Five(Room),
              'SIX': Six(Room),
              'SEVEN': Seven(Room),
              'EIGHT': Eight(Room),
              'NINE': Nine(Room),
              'EXIT': Exit(Room),
    }

    # room_state dictionay holds certain key names in the
    # GetObject.object_status[] dictionary.
    #
    # The values here are in the form of a tuple because when they are
    # plugged in to the GetObject.object_status[] dictionary via a variable
    # which will be assigned their value they need to be hashable.
    #
    # Only room FIVE has two key names. But since the code is expecting a two
    # value tuple in one specific spot, I had to put in a second 'VALUE_NOT_USED'
    # value to compensate for that. It's a hack but doing things this Way
    # eliminated two dozen if statements that previously existed.
    room_state = {
                       'ONE': ('book_got', 'VALUE_NOT_USED'),
                       'TWO': ('sign_one_got', 'VALUE_NOT_USED'),
                       'THREE': ('rope_got', 'VALUE_NOT_USED'),
                       'FOUR': ('mirror_used', 'VALUE_NOT_USED'),
                       'FIVE': ('pirate_gone', 'can_got'),
                       'SIX': ('sign_two_got', 'VALUE_NOT_USED'),
                       'SEVEN': ('knife_got', 'VALUE_NOT_USED'),
                       'EIGHT': ('mirror_got', 'VALUE_NOT_USED'),
                       'NINE': ('bear_eating', 'VALUE_NOT_USED'),
                       'EXIT': ('bear_eating', 'VALUE_NOT_USED')
    }

    def __init__(self, room):
        '''
        Constructor for Map class.

        Attributes:
            room (str): The current room the player is in.
        '''
        self.room = room

    def play(self):
        '''
        Map.play() is the engine that moves the player around in the game
        from room to room using an infinite loop.

        Map.play() takes whatever the value is of the current room and
        checks to see if the player has been there before. If they haven't,
        the function calls the enter() function in the current room's class
        that is specifically for first visits.

        If they have been there before the function calls the re_enter()
        function if that room's object(s) has/have not had their status change
        from the default status. If the value of the status has changed from
        default the empty_room() function may be called or, if it's rooms FIVE
        or NINE custom re-entry functions will be called depending on the
        scenario being played out.

        When that function returns, the Engine.play() function is called so
        that the player can enter a command. If the command entered is
        anything other than an available door name, Engine.prompt() takes
        further action (see that function's docstring for more information)
        and Engine.prompt() will continue to run things until an available door
        name is entered. At which point the door name's string is brought back
        to Map.play() where it becomes the current door name and the loop
        starts over.

        Parameters:
            room (Map): The current room string.
        '''

        while self.room.lower():

            # Retrieves room class based on the current room value
            next_room = Map.scenes[self.room.upper()]

            # Sets room_state to whatever values exist in the Map.room_state[]
            # dictionary for the room the player is entering. Each value is
            # the same string value used in the GetObject.object_status[]
            # dictionary.
            room_state = Map.room_state[self.room.upper()]

            # Runs the enter function for the current room
            # if a room is being entered for the firts time
            if Room.first_time[self.room.lower()] == "yes":

                # If the player enters room FOUR after using the mirror in room
                # ONE, Cthulhu will be defeated.
                if GetObject.object_status[room_state[0]] == 1 and self.room.lower() == "four":
                    next_room_key = next_room.mirror_attack()
                    GetObject.object_status['cthulhu_fled'] = 1

                # If the player tries to exit the game and the bear is not eating run
                # Exit.exit_blocked() and also reset self.room to "nine".
                elif GetObject.object_status[room_state[0]] == 0 and self.room.lower() == "exit":
                    next_room_key = next_room.exit_blocked()
                    self.room = "nine"

                # Otherwise the player will enter any room, including FOUR, for the first time.
                else:
                    next_room_key = next_room.enter()

            # If this is a return visit to a room
            else:
                # Room FIVE is a special case because you have to get rid of
                # the pirate before you can grab the can of honey. This if
                # starts by checking to see if the pirate is gone from the room.
                if GetObject.object_status[room_state[0]] == 1 and self.room.lower() == "five":

                    # If the can has already been gotten by the player
                    if GetObject.object_status[room_state[1]] == 1:
                        next_room_key = next_room.empty_room()
                    # If the can is still on the ground
                    else:
                        next_room_key = next_room.can_on_ground()

                # Room NINE is a special case because there are two room re-entry scenarios;
                # one for if the bear is eating and one for if he's not eating.
                elif GetObject.object_status[room_state[0]] == 1 and self.room.lower() == "nine":
                    next_room_key = next_room.re_enter_bear_eating()

                # All other rooms have only one entry and one re-entry scenario so this
                # if statement covers all of them
                elif GetObject.object_status[room_state[0]] == 1:
                    next_room_key = next_room.empty_room()
                else:
                    next_room_key = next_room.re_enter()

            # Retrieves the available doors to use based on the current room
            doors = Room.maze[self.room.upper()]
            door_list = ', '.join(doors)
            print("You see the following doors leading out of the room:", door_list)

            # Instantiate Engine() using the current room and the number of
            # available doors in that room
            command = Engine(self.room.lower(), doors)

            # next_room_key will get the return value from prompt() for
            # what the next door to go through will be
            next_room_key = command.prompt()
            self.room = next_room_key.lower()

start_room = Map('One')
start_room.play()
