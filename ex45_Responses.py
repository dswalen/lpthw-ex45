from textwrap import dedent

class UseObjectResponses(object):
    '''
    UseObjectResponses class. Contains all string responses for the player
    using any object in the game. Each response takes the form of a function
    which consists entirely of a print statement and a text string. These
    responses are called from UseObject.using() in ex45_API.py. Consult the
    docstring for that class as to how it works.
    '''
    def do_not_have_item(self):
        '''
        Generic response if the player tries to use an item not in their
        posession.
        '''
        print(dedent("""
                     You don't have that item to use.
                     """))

    def no_use_for_item(self):
        '''
        Generic response if the player tries to use an item in any room where
        nothing would happen if they did.
        '''
        print(dedent("""
                     There's no reason to use that in this room.
                     """))

    def use_mirror(self):
        '''
        Generic response if the player tries to use the mirror in any room
        other than room ONE.
        '''
        print(dedent("""
                     There might be a reason to do that but not in this room.
                     """))

    def use_mirror_room_one(self):
        '''
        Response if the player tries to use the mirror in room ONE.
        '''
        print(dedent("""
                     You take the mirror in your hands and hold it out in
                     front of you; mirror side facing out. Ahhhh...I see
                     where you're going with this.
                     """))

    def use_book(self):
        '''
        Response if the player tries to use the book in any room
        other than room FIVE.
        '''
        print(dedent("""
                     You don't see anything in the book that pertains to this room.
                     """))

    def use_book_room_five(self):
        '''
        Response if the player uses the book in room FIVE.
        '''
        print(dedent("""
                     Hmmmm...according to the book a Boolean expression might
                     work in this particular situation.
                     """))

    def use_sign(self):
        '''
        Response if the player uses a sign by itself.
        '''
        print(dedent("""
                     Fake news!
                     """))

    def use_rope_alone(self):
        '''
        Response if the player uses the rope without using it with a sign
        '''
        print(dedent("""
                     Use the rope with what?
                     """))

    def no_got_rope(self):
        '''
        Response if the player tries to use the rope but doesn't have it.
        '''
        print(dedent("""
                     You don't have any rope to use.
                     """))

    def no_got_knife(self):
        '''
        Response if the player tries to use the knife but doesn't have it.
        '''
        print(dedent("""
                     You don't have a knife to use.
                     """))

    def all_knotted_up(self):
        '''
        Response if the player tries to tie a rope to a sign but has already
        tied up all the signs.
        '''
        print(dedent("""
                     Everything is already tied in knots!
                     """))

    def knot_already_tied(self):
        '''
        Response if the player tries to use the rope with a sign they
        already used the rope with.
        '''
        print(dedent("""
                     You already tied a knot around that sign.
                     """))

    def use_rope_sign_one(self):
        '''
        Response if the player ties a knot around sign_one.
        '''
        print(dedent("""
                     You tied a knot around the \"The Earth Is Flat!\" sign.
                     """))

    def use_rope_sign_two(self):
        '''
        Response if the player ties a knot around sign_two.
        '''
        print(dedent("""
                     You tied a knot around the \"The Moon Landing Was Faked!\" sign.
                     """))

    def tied_both_signs(self):
        '''
        Response if the player has tied a knot around both signs.
        '''
        print(dedent("""
                     You now have two signs with knots around them.
                     """))

    def use_knife_pirate(self):
        '''
        Response if the player tries to use the knife on the pirate
        '''
        print(dedent("""
                     You pull out your Ginsu 2000 and point it at Guybrush
                     Threepwood(r); Mighty Pirate!(tm)(c). Guybrush Threepwood(r);
                     Mighty Pirate!(tm)(c) pulls out his rapier. You see the words
                     \"Ginsu Killer\" engraved on it.

                     Guybrush Threepwood(r); Mighty Pirate!(tm)(c) cuts your
                     puny TV knife in two and proceeds to cut a tic-tac-toe
                     game on hour chest.

                     The good news? It was a cat's game and he didn't win. The
                     bad news? You bled out. Good Job!

                     (\'Guybrush Threepwood(r); Mighty Pirate!(tm)(c)\' is a
                     registered trademark of LucasArts(tm)/Disney(tm) and is
                     used completely without permission. So there.)
                     """))
        exit(0)

    def use_signs(self):
        '''
        Generic response if the player tries to use both knotted signs in
        any room other than room FIVE.
        '''
        print(dedent("""
                     There's obviously a reason to use these but this room
                     doesn't seem like the place for it.
                     """))

    def use_signs_room_five(self):
        '''
        Response if the player tries to use both knotted signs in room FIVE
        '''
        print(dedent("""
                     You hand the knotted signs to Guybrush Threepwood(r);
                     Mighty Pirate!(tm)(c).

                     Guybrush Threepwood(r); Mighty Pirate!(tm)(c), stares at
                     them for a moment and then speaks...

                     \'A knot around a sign saying \"The Earth Is Flat!\" and a
                     knot around a sign saying \"The Moon Landing Was Faked!\"...

                     You have obviously played \'The Secret of Monkey
                     Island\'(r)(tm)(c).

                     ...knot \"The Earth is Flat\" and knot \"The Moon Landing
                     Was Faked!\"?!?!

                     Shiver me timbers! That\'s TRUE!

                     Time for a grog break. I\'m outta here!\'

                     Guybrush Threepwood\'(r); Mighty Pirate!(tm)(c), gets up
                     and walks out of the room. His departure reveals a giant
                     can of honey which he had been sitting on.

                     (\'Guybrush Threepwood(r); Mighty Pirate!(tm)(c)\', and
                     \'The Secret of Monkey Island(r)(tm)(c)\' are registered
                     trademarks of LucasArts(tm)/Disney(tm) and are used
                     completely without permission. So there.)
                     """))

    def use_can(self):
        '''
        Response if the player tries to use the can in room NINE and it has
        been opened.
        '''
        print(dedent("""
                     You toss the can of honey across the room. The bear chases
                     after it and starts eating out of it!
                     """))

    def bear_already_fed(self):
        '''
        Response if the player tries to toss a can they don't have to the bear
        who is already eating.
        '''
        print(dedent("""
                     Use what can of honey? You already fed him!
                     """))

    def use_can_not_open(self):
        '''
        Response if the player tries to use the can if it has not been opened.
        '''
        print(dedent("""
                     There's no point in using an unopened can.
                     """))

    def can_already_opened(self):
        '''
        Response if the player tries to use the knife with the can if it has
        already been opened.
        '''
        print(dedent("""
                     You already opened the can.
                     """))

    def use_knife_can(self):
        '''
        Response if the player tries to use the knife to open the can.
        '''
        print(dedent("""
                     You use the knife to cut open the can of honey.
                     """))

    def use_knife_nothing(self):
        '''
        Response if the player tries to use the knife and there's nothing to
        use it on.
        '''
        print(dedent("""
                     Use the knife with what?
                     """))

    def use_knife_no_can(self):
        '''
        Response if the player tries to use the knife but doesn't have the can.
        '''
        print(dedent("""
                     You don't have a can of honey.
                     """))

    def pirate_gone_with_signs(self):
        '''
        Response if the player tries to use the knotted signs after having
        sent the pirate away.
        '''
        print(dedent("""
                     You don't have them anymore. Guybrush Threepwood(r);
                     Mighty Pirate!(tm)(c) took them with him when he left.

                     (\'Guybrush Threepwood(r); Mighty Pirate!(tm)(c)\' is a
                     registered trademark of LucasArts(tm)/Disney(tm) and is
                     used completely without permission. So there.)
                     """))

    # The object_use_dict dictionary is a database of key pairs consisting of
    # tuples made up of object status variable names (in string form) that are
    # used by the GetObject.object_status() dictionary and
    # UseObjectResponses() functions.
    #
    # For each key in the dictionary the first two tuple values are object
    # status variable names used in the GetObject.object_status() dictionary.
    # The second two tuple values are functions used in UseObjectResponse().
    #
    # The convention that is being followed for this dictionary is the first
    # object in the tuple is a variable and the third object is a function that
    # would be called if the first object's value is "1" (values are normally "0"
    # and "1" with a default of "0" but knotted_signs has been amended to also
    # have a value of "3" to plug a logic hole). The second object in the
    # tuple is also a variable and the fourth object in the tuple is a
    # different function that will or will not be called in UseObject.using()
    # depending on the second variable's value.


    object_use_dict = {
                       # room ONE
                       'mirror': ('mirror_got', 'mirror_used',
                       use_mirror_room_one, use_mirror),

                       # room FIVE
                       '\"Learn Python The Hard Way\"': ('book_got',
                       'pirate_gone', use_book_room_five, use_book),

                       'knife': ('knife_got', 'pirate_gone', use_knife_pirate,
                       no_got_knife),

                       'knotted signs': ('knotted_signs', 'pirate_gone',
                       use_signs_room_five, use_signs),

                       # room NINE
                       'opened can of honey': ('bear_eating', 'can_opened',
                       use_can, use_can_not_open),

                       'can of honey': ('can_got', 'can_opened', use_knife_no_can,
                       use_can_not_open),

                       # anywhere
                       'rope with \"The Earth Is Flat!\" sign': ('rope_got',
                       'sign_one_got', use_rope_sign_one, knot_already_tied),

                       '\"The Earth Is Flat!\" sign with rope': ('rope_got',
                       'sign_one_got', use_rope_sign_one, knot_already_tied),

                       'rope with \"The Moon Landing Was Faked!\" sign':
                       ('rope_got', 'sign_two_got', use_rope_sign_two,
                       knot_already_tied),

                       '\"The Moon Landing Was Faked!\" sign with rope':
                       ('rope_got', 'sign_two_got', use_rope_sign_two,
                       knot_already_tied),

                       'knife with can of honey': ('knife_got', 'can_got',
                       use_knife_can, use_knife_no_can),

                       'can of honey with knife': ('knife_got', 'can_got',
                       use_knife_can, use_knife_no_can),

                       '\"The Earth Is Flat!\" sign': ('sign_one_got',
                       'sign_one_got', use_sign, do_not_have_item),

                       '\"The Moon Landing Was Faked!\" sign': ('sign_two_got',
                       'sign_two_got', use_sign, do_not_have_item),

                       'rope': ('rope_got', 'knotted_signs', use_rope_alone,
                       no_got_rope)
    }
