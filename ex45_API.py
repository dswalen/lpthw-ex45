from textwrap import dedent
from ex45_Responses import UseObjectResponses

class Room(object):
    '''
    Parent class for all room subclasses

    Attributes:
        room (str): The current room the player is in.
    '''

    # Tracks door availability based on room player is in
    maze = {
            'ONE':['Two', 'Four'],
            'TWO':['One', 'Three', 'Five'],
            'THREE':['Two', 'Six'],
            'FOUR':['One', 'Seven'],
            'FIVE':['Two', 'Six', 'Eight'],
            'SIX':['Three', 'Five', 'Nine'],
            'SEVEN':['Four'],
            'EIGHT':['Five', 'Nine'],
            'NINE':['Six', 'Eight', 'Exit'],
            'EXIT':['Six', 'Eight', 'Exit']
    }

    # This dictionary tracks whether a room has been visited for the first
    # time or not. Values will change from "yes" to "no" after they've been
    # visited. Based on the current value this makes it makes it possible for
    # there to be two different room entrance functions.
    first_time = {
                  'one':'yes',
                  'two':'yes',
                  'three':'yes',
                  'four':'yes',
                  'five':'yes',
                  'six':'yes',
                  'seven':'yes',
                  'eight':'yes',
                  'nine':'yes',
                  'exit':'yes'
    }

    def __init__(self, room):
        '''
        Constructor for Room class.

        Attributes:
            room (str): The current room the player is in.
        '''
        self.room = room

    def room_visited(self, room):
        '''
        Function to change a room's first time visit status from yes to no.

        Parameters:
            room  (Room): The current room string.
        '''
#        self.room = room
        Room.first_time[room.lower()] = "no"

    def empty_room(self):
        '''
        Function to enter any room that is empty. This function is inherited
        by all subclasses.
        '''
        print(dedent("""
                     You enter a room with nothing in it.
                     """))

    def look_empty_room(self):
        '''
        Function to look in any room that is empty. This function is inherited
        by all subclasses.
        '''
        print(dedent("""
                     There's nothing in this room.
                     """))
        return self.room

class One(Room):
    '''
    Room One subclass. Contains functions pertaining to things that can
    occur in room ONE (e.g. Enter, Re-Enter)
    '''
    def enter(self):
        '''
        Function for entering room ONE for the first time.
        '''
        print(dedent("""
                     You have fallen into a cavern. You get up and notice a copy
                     of the book \"Learn Python The Hard Way\" in a corner.
                     It is very dusty.

                     Now might be a good time to call for \"help\"!
                     """))
        self.room_visited("one")

    def re_enter(self):
        '''
        Function for re-entering room ONE.
        '''
        print(dedent("""
                     You enter a room which has a dusty book labeled \"Learn
                     Python The Hard Way\" sitting in a corner.
                     """))
    def look(self):
        '''
        Function for looking around room ONE.
        '''
        print(dedent("""
                     You see a book labeled \"Learn Python The Hard Way\"
                     sitting in a corner.
                     """))

class Two(Room):
    '''
    Room Two subclass. Contains functions pertaining to things that can
    occur in room TWO (e.g. Enter, Re-Enter)
    '''

    def enter(self):
        '''
        Function for entering room TWO for the first time.
        '''
        print(dedent("""
                     You enter a room. There is a sign on a wall...

                     \"The Earth Is Flat!\"
                     \"- A public service message from The Flat Earth Society\"
                     """))
        self.room_visited("two")

    def re_enter(self):
        '''
        Function for re-entering room TWO.
        '''
        print(dedent("""
                     You enter a room which has a sign that says \"The Earth
                     Is Flat!\" hanging on a wall.
                     """))

    def look(self):
        '''
        Function for looking around the room.
        '''
        print(dedent("""
                     In the room you see the \"The Earth Is Flat!\" sign.
                     """))

class Three(Room):
    '''
    Room Three subclass. Contains functions pertaining to things that can
    occur in room THREE (e.g. Enter, Re-Enter)
    '''

    def enter(self):
        '''
        Function for entering room THREE for the first time.
        '''
        print(dedent("""
                     You enter a room. It is empty except for some rope lying
                     on the ground.
                     """))
        self.room_visited("three")

    def re_enter(self):
        '''
        Function for re-entering room THREE.
        '''
        print(dedent("""
                     You enter a room that has some rope lying on the ground.
                     """))

    def look(self):
        '''
        Function for looking around the room.
        '''
        print(dedent("""
                     You see some rope lying on the ground.
                     """))

class Four(Room):
    '''
    Room Four subclass. Contains functions pertaining to things that can
    occur in room FOUR (e.g. Enter, Re-Enter)
    '''

    def enter(self):
        '''
        Function for entering room FOUR for the first time. The player
        always dies in this function.
        '''
        print(dedent("""
                     You enter the room and see a door on the other side.
                     Suddenly there's a puff of smoke and you smell an odor most
                     foul. Out of the smoke appears The Great, Evil Cthulhu!

                     He, it, whatever stares at you and you go insane!

                     Do you [flee] for your life or [eat] your head?
                     """))
        choice = input("> ")
        if "flee" in choice:
            print(dedent("""
                         Too late! You're already insane so you just wander
                         around in a vegetative state until you die!

                         Good Job!
                         """))
            exit(0)
        elif "eat" in choice:
            print(dedent("""
                         Well that was tasty! Good job!
                         """))
            exit(0)
        else:
            print(dedent("""
                         The Great, Evil, Cthulhu becomes annoyed with your
                         terrible syntax and yanks your arms from their sockets!

                         Good job!
                         """))
            exit(0)

    def re_enter(self):
        '''
        Function for re-entering the room.
        '''
        print(dedent("""
                     You enter a room with nothing in it.
                     """))

    def mirror_attack(self):
        '''
        Function for defeating Cthulhu.
        '''
        print(dedent("""
                     You enter the room with the mirror out in front of you
                     so that you can't see The Great, Evil Cthulhu.

                     He, it, whatever sees he, it, whatever in the mirror and
                     goes insane!

                     The former Great, Evil Cthulhu; now The Lesser, Insane,
                     Mind of Jello Cthulhu, wanders out of the room in a
                     vegitative state!
                     """))
        self.room_visited("four")

class Five(Room):
    '''
    Room Five subclass. Contains functions pertaining to things that can
    occur in room FIVE (e.g. Enter, Re-Enter)
    '''

    def enter(self):
        '''
        Function for entering room FIVE for the first time.
        '''
        print(dedent("""
                     You enter a room. There is a pirate sitting in it. You know
                     he's a pirate because he smells. The pirate looks at you
                     and speaks.

                     Ahoy there matey! I am Guybrush Threepwood(r); Mighty
                     Pirate!(tm)(c)

                     I\'m an old salt of the sea. For many years have I pursued
                     that cursed LeChuck(r)(tm)(c), my brother, over the high
                     seas...

                     ...even though he turned me into a child in the second game
                     and that dangling plot point was never addressed in any
                     subsequent game...I mean REALLY! How could they screw
                     that up?!?!?

                     I'm sorry, I digressed...

                     Anyway, LeChuck(r)(tm)(c) has kidnapped my wife, the
                     lovely Elaine Threepwood (nee Marley)(r)(tm)(c) and I have
                     pursued him to hell and back...

                     and then back to hell again...

                     and then back but only after a detour to limbo and then to
                     heaven but that was because I made a wrong turn and then
                     back to hell and now I'm here...

                     And I'm not moving from this spot until I figure out where
                     to go next.

                     And nobody's moving me from this spot unless their code
                     is true!

                     You notice that Guybrush Threepwood(r); Mighty Pirate!(tm)(c),
                     is sitting on something but you cannot make out what it is
                     at that distance. And given that smell, trust me, you
                     DON\'T want to get any closer!

                     (\'Guybrush Threepwood(r); Mighty Pirate!(tm)(c)\',
                     \'LeChuck(r)(tm)(c)\', and \'Elaine Threepwood
                     (nee Marley)(r)(tm)(c)\' are registered trademarks of
                     LucasArts(tm)/Disney(tm) and are used completely without
                     permission. So there.)
                     """))
        self.room_visited("five")

    def re_enter(self):
        '''
        Function for re-entering room FIVE while the pirate is there.
        '''
        print(dedent("""
                     You enter a room where you see Guybrush Threepwood(r),
                     Mighty Pirate!(tm)(c) He is sitting on something but you
                     cannot get any closer to tell what it is because he smells.

                     (\'Guybrush Threepwood(r); Mighty Pirate!(tm)(c)\' is a
                     registered trademark" of LucasArts(tm)/Disney(tm) and is
                     used completely without permission. So there.)
                     """))

    def look(self):
        '''
        Function for looking around room FIVE while the pirate is there.
        '''
        print(dedent("""
                     You see Guybrush Threepwood(r), Mighty Pirate!(tm)(c) He is
                     sitting on something.

                     (\'Guybrush Threepwood(r); Mighty Pirate!(tm)(c)\' is a
                     registered trademark" of LucasArts(tm)/Disney(tm) and is
                     used completely without permission. So there.)
                     """))

    def look_can(self):
        '''
        Function for looking around room FIVE when the can is exposed.
        '''
        print(dedent("""
                     You see a can of honey on the ground.
                     """))

    def can_on_ground(self):
        '''
        Function for re-entering room FIVE with the pirate gone and the can
        of honey is sitting on the floor still.
        '''
        print(dedent("""
                     You enter a room which has a can of honey sitting on
                     the ground.
                     """))

    def pirate_not_gone(self):
        '''
        Function that prevents the player from getting the can of honey while
        the pirate is still sitting on it.
        '''
        print(dedent("""
                You can't get that as long as Guybrush Threepwood(r); Mighty Pirate!(tm)(c)
                is sitting on it. Make him go away.

                (\'Guybrush Threepwood(r); Mighty Pirate!(tm)(c)\', is a registered trademark
                of LucasArts(tm)/Disney(tm) and is used completely without permission. So there.)
                """))

    def taunt(self):
        '''
        Function for taunting the pirate.
        '''
        print(dedent("""
                     Guybrush Threepwood(r); Mighty Pirate!(tm)(c), looks at
                     you dismissively...

                     \"Avast ye! I have sailed across the mighty seas spending
                     years honing my insults. Ya think a bilge sucking scallywag
                     such as yourself is in my league? Ye can't hornswaggle me.
                     Ye can't insult me. I give no quarter...

                     Now begone ya lily-livered landlubber before I keelhaul ya!\"

                     (\'Guybrush Threepwood(r); Mighty Pirate!(tm)(c)\' is a
                     registered trademark of LucasArts(tm)/Disney(tm) and is
                     used completely without permission. So there.)

                     Hmmmm...doesn't look like taunting is going to make him
                     get up. Better think of another tactic.
                     """))

class Six(Room):
    '''
    Room Six subclass. Contains functions pertaining to things that can
    occur in room SIX (e.g. Enter, Re-Enter)
    '''

    def enter(self):
        '''
        Function for entering room SIX for the first time.
        '''
        print(dedent("""
                     You enter a room. There is a sign on a wall...

                     \"The Moon Landing Was Faked!\"
                     \"- A public service message from The Flat Earth Society\"
                     """))
        self.room_visited("six")

    def re_enter(self):
        '''
        Function for re-entering room SIX.
        '''
        print(dedent("""
                     You enter a room that has a sign hanging on a wall that
                     says \"The Moon Landing Was Faked!\".
                     """))

    def look(self):
        '''
        Function for looking around room SIX.
        '''
        print(dedent("""
                     In the room you see the \"The Moon Landing Was Faked!\" sign.
                     """))

class Seven(Room):
    '''
    Room Seven subclass. Contains functions pertaining to things that can
    occur in room SEVEN (e.g. Enter, Re-Enter)
    '''

    def enter(self):
        '''
        Function for entering room SEVEN for the first time.
        '''
        print(dedent("""
                     You enter a room. You see that it is empty except for a
                     knife which was left lying on the ground. The knife has
                     the words \"Ginsu 2000\" printed on the side. All we need
                     now is a can to cut followed by a tomato!
                     """))
        self.room_visited("seven")

    def re_enter(self):
        '''
        Function for re-entering room SEVEN.
        '''
        print(dedent("""
                     You enter a room which has a knife lying on the ground.
                     """))


    def look(self):
        '''
        Function for looking around room SEVEN.
        '''
        print(dedent("""
                     You see a knife on the ground.
                     """))

class Eight(Room):
    '''
    Room Eight subclass. Contains functions pertaining to things that can
    occur in room EIGHT (e.g. Enter, Re-Enter)
    '''

    def enter(self):
        '''
        Function for entering room EIGHT for the first time.
        '''
        print(dedent("""
                     You enter a room. You see a mirror hanging on a wall.

                     You go up to the mirror and look at yourself. Damn you
                     look good!
                     """))
        self.room_visited("eight")

    def re_enter(self):
        '''
        Function for re-entering room EIGHT.
        '''
        print(dedent("""
                     You enter a room that has a mirror hanging on a wall.
                     """))

    def look(self):
        '''
        Function for looking around room EIGHT.
        '''
        print(dedent("""
                     You see a mirror hanging on a wall.
                     """))

class Nine(Room):
    '''
    Room Nine subclass. Contains functions pertaining to things that can
    occur in room NINE (e.g. Enter, Re-Enter)
    '''

    def enter(self):
        '''
        Function for entering room NINE for the first time.
        '''
        print(dedent("""
                     You enter a room. There is a giant angry bear in it.
                     He looks hungry. I wouldn't hang around.
                     """))
        self.room_visited("nine")

    def re_enter(self):
        '''
        Function for re-entering room NINE.
        '''
        print(dedent("""
                     You enter a room which has a giant angry bear in it.
                     """))

    def re_enter_bear_eating(self):
        '''
        Function for re-entering room NINE while the bear is eating.
        '''
        print(dedent("""
                     You enter a room which has a giant angry bear in it. He is
                     busy devouring a can of honey. I'd let him keep doing
                     that if I were you.
                     """))

    def look(self):
        '''
        Function for looking around room NINE.
        '''
        print(dedent("""
                     You see a giant angry bear. He's blocking the Exit.
                     """))

    def look_bear_eating(self):
        '''
        Function for looking around room NINE while the bear is eating.
        '''
        print(dedent("""
                     You see a giant agnry bear. He is busy eating.
                     """))

    def taunt(self):
        '''
        Function for taunting the bear.
        '''
        print(dedent("""
                     You jump up and down waving your arms wildly. The bear
                     thinks you want to play!

                     He lumbers over and jumps on you crushing your chest in!

                     Your guts squirt out through your ribs! Good job!
                     """))
        exit(0)

    def taunt_eating(self):
        '''
        Function for taunting the bear while he's eating.
        '''
        print(dedent("""
                     You kick over the can of honey while the bear is eating
                     it. He didn't like that.

                     The bear rips your face off! Good Job!
                     """))
        exit(0)

class Exit(Room):
    '''
    Room Exit subclass. Contains functions for exiting the maze.
    '''

    def enter(self):
        '''
        Function for entering the exiting the maze.
        '''
        print("\nCongratulations...you escaped!")
        exit(0)

    def exit_blocked(self):
        '''
        Function for preventing the player from using the Exit in room NINE
        because the bear is blocking the way.
        '''
        print(dedent("""
                     The bear is blocking the Exit door! You can't get past him.
                     """))

class GetObject(object):
    '''
    GetObject class. Contains all functions and data pertaining to the player
    acquiring things in the game.

    Attributes:
        room (str): The current room the player is in.
        words (list): The remainder of the list of words from the original
                  player entered command. Contains all the remaining words
                  from the original player entered command once "Get" had
                  been popped off.
    '''
    # Pouch represents the List of items the player can acquire and carry
    # throughout the game. Defaults to empty.
    pouch = []

    # Dictionary to keep track of the status of various things in the game.
    # Values will default to 0 and change to 1 as the game progresses.
    object_status = {
                     'book_got': 0,
                     'sign_one_got': 0,
                     'sign_two_got': 0,
                     'rope_got': 0,
                     'knife_got': 0,
                     'mirror_got': 0,
                     'mirror_used': 0,
                     'can_got': 0,
                     'can_opened': 0,
                     'cthulhu_fled': 0,
                     'pirate_gone': 0,
                     'knotted_sign_one': 0,
                     'knotted_sign_two': 0,
                     'knotted_signs': 0,
                     'bear_eating': 0
    }

    # Dictionary object_get_dict stores lists of information for the rooms in
    # the game that have objects the user needs to pick up. Rooms FOUR and NINE
    # have no objects so are not included. The key for each pair is the
    # current room name
    #
    # The string convention for each list in the key:value pair is the first
    # string is the room's object and the second string is the corresponding
    # object.status for that room object. Except for room FIVE which has three
    # strings because it is treated differently in the code from the other rooms
    # that have objects.
    #
    # Commands.get() will go through this dict to acquire the list based on the
    # current room the player is in.
    object_get_dict = {
                      'ONE': ['\"Learn Python The Hard Way\"', 'book_got'],
                      'TWO': ['\"The Earth Is Flat!\" sign', 'sign_one_got'],
                      'THREE': ['rope', 'rope_got'],
                      'FOUR': ['VALUE_NOT_USED', 'VALUE_NOT_USED'],
                      'FIVE': ['can of honey', 'pirate_gone', 'can_got'],
                      'SIX': ['\"The Moon Landing Was Faked!\" sign', 'sign_two_got'],
                      'SEVEN': ['knife', 'knife_got'],
                      'EIGHT': ['mirror', 'mirror_got'],
                      'NINE': ['VALUE_NOT_USED', 'VALUE_NOT_USED'],
                      'EXIT': ['VALUE_NOT_USED', 'VALUE_NOT_USED']
    }

    def __init__(self, room, words):
        '''
        Constructor for GetObject() class.

        Attributes:
            room (str): The current room the player is in.
            words (list): The remainder of the list of words from the original
                          player entered command. Contains all the remaining
                          words from the original player entered command once
                          "Get" had been popped off.
        '''
        self.room = room
        self.words = words

    def string_convert(self, words):
        '''
        Function to reconstitute the remainder of the words list as a string
        after the command word had been popped off.

        Parameters:
            words (list): The remainder of the list of words from the original
                          player entered command. Contains all the remaining
                          words from the original player entered command once
                          "Get" has been popped off.

        Returns:
            String containing the reconstituted list.
        '''
        string = " "
        object = (string.join(words))
        object = object.strip()
        return object

    def grab(self, room, words):
        '''
        Function to pick up an object in a room.

        Parameters:
            room (str): The current room the player is in.
            words (list): The remainder of the list of words from the original
                          player entered command. Contains all the remaining
                          words from the original player entered command once
                          "Get" has been popped off.
        '''

        # Reconstitute the words list back to a string.
        object = self.string_convert(words)
        # Sets room_contents to the list of strings based on the current room
        # the player is in.
        room_contents = GetObject.object_get_dict[room.upper()]
        # If the player failed to add an object name after their "get" command.
        if words == []:
            print("\nGet what? You didn't specify anything.")
        else:
            # If the player is in rooms FOUR or NINE there are no ojbects to get
            if room == 'four' or room == 'nine':
                print ("\nThere's nothing to grab in this room.")
            # Room FIVE is a special case because the code needs to test to
            # see if the pirate has vacated the room before the player is allowed
            # to grab the can of honey.
            elif room == 'five':
                if object == room_contents[0]:

                    # If the pirate has not left the room, instantiate the
                    # Five() class and then call the pirate_not_gone function.
                    if GetObject.object_status[room_contents[1]] == 0:
                        pirate_here = Five(room)
                        pirate_here.pirate_not_gone()
                    # If the pirate's gone but the player already has the
                    # object they are trying to get.
                    elif GetObject.object_status[room_contents[2]] == 1:
                        print("\nYou already have it!")
                    # If the can of honey has been picked up and opened, the
                    # player should be prevented from picking up another can.
                    elif GetObject.object_status['can_opened'] == 1:
                        print("\nThere are no cans left to get.")
                    # Otherwise add the object to players pouch and set the ojbect's
                    # object_status dictionary value to 1 indicating it's gotten.
                    else:
                        GetObject.object_status[room_contents[2]] = 1
                        GetObject.pouch.append(room_contents[0])
                        print("\nYou got it!")
                # If the signs have been knotted, there's no rope left.
                elif object == 'rope' and GetObject.object_status['knotted_signs'] == 1:
                        print("\nThere's no rope to get.")
                # Otherwise there was a typo and this needs to be done over.
                else:
                    print("\nGet what?. I didn't understand what you wrote.")

            # If not in room FIVE
            elif object == room_contents[0]:
                # If the player already has the object they are trying to get.
                if GetObject.object_status[room_contents[1]] == 1:
                    # If the signs have been knotted, there's no rope left.
                    if object == 'rope' and GetObject.object_status['knotted_signs'] == 1:
                        print("\nThere's no rope to get.")
                    else:
                        print("\nYou already have it!")
                # Otherwise add the object to players pouch and set the ojbect's
                # object_status dictionary value to 1 indicating it's gotten.
                else:
                    GetObject.object_status[room_contents[1]] = 1
                    GetObject.pouch.append(room_contents[0])
                    print("\nYou got it!")
            # If the signs have been knotted, there's no rope left.
            elif object == 'rope' and GetObject.object_status['knotted_signs'] == 1:
                    print("\nThere's no rope to get.")
            # Otherwise there was a typo and this needs to be done over.
            else:
                print("\nGet what? I didn't understand what you wrote.")

class UseObject(object):
    '''
    UseObject class. Contains all command functions pertaining to the player
    using things in the game. UseObject.using() is the function that does all
    the heavy lifting.

    Attributes:
        room (str): The current room the player is in.
        words (list): The remainder of the list of words from the original
                  player entered command. Contains all the remaining words
                  from the original player entered command once "Use" had
                  been popped off.
    '''
    def __init__(self, room, words):
        '''
        Constructor for UseObject() class.

        Attributes:
            room (str): The current room the player is in.
            words (list): The remainder of the list of words from the original
                          player entered command. Contains all the remaining
                          words from the original player entered command once
                          "Use" had been popped off.
        '''
        self.room = room
        self.words = words

    def string_convert(self, words):
        '''
        Function to reconstitute the remainder of the words list as a string
        after the command word had been popped off.

        Parameters:
            words (list): The remainder of the list of words from the original
                          player entered command. Contains all the remaining
                          words from the original player entered command once
                          "Get" has been popped off.

        Returns:
            String containing the reconstituted list.
        '''
        string = " "
        object = (string.join(words))
        object = object.strip()
        return object

    def pouch_index_search(self, search_string):
        '''
        Function to search the pouch list for an item and replace it with a
        different item. The parameter search_string is used to compare against
        various strings. If a match is found, the string is popped off of
        the pouch list. Then a new string is added to the pouch list. This
        function is only called from UseObject.using() if the player
        successfully uses one object with another object.

        Example: if the player uses the rope with a sign, the original sign
        string in the pouch is removed and a new sign string that references a
        rope being tied around in is added to pouch.

        Parameters:
            search_string (str): The string of the command object passed from
            UseObject.using().
        '''

        if search_string == '\"The Earth Is Flat!\" sign':
            # if sign two is already knotted the code will remove "Earth flat"
            # sign and the "Knotted Moon Landing" sign from inventory and
            # add "Knotted Signs" in their place
            if GetObject.object_status['knotted_sign_two'] == 1:
                for i, j in enumerate(GetObject.pouch):
                    if j == search_string:
                        GetObject.pouch.pop(i)
                for i, j in enumerate(GetObject.pouch):
                    if j == 'knotted \"The Moon Landing Was Faked!\" sign':
                        GetObject.pouch.pop(i)
                for i, j in enumerate(GetObject.pouch):
                    if j == 'rope':
                        GetObject.pouch.pop(i)
                GetObject.pouch.append('knotted signs')
                # Set the status for knotted_signs to 1.
                GetObject.object_status['knotted_signs'] = 1

            # otherwise if the player hasn't knotted sign two yet, searches
            # for "Earth flat" sign and removes it from inventory and adds
            # "knotted 'Earth flat'" sign.
            else:
                for i, j in enumerate(GetObject.pouch):
                    if j == search_string:
                        GetObject.pouch.pop(i)
                GetObject.pouch.append('knotted \"The Earth Is Flat!\" sign')

        elif search_string == '\"The Moon Landing Was Faked!\" sign':
            # if sign one is already knotted the code will remove "Moon
            # Landing" sign and "Knotted Earth flat" sign from the inventory
            # and add "Knotted Signs" in their place.
            if GetObject.object_status['knotted_sign_one'] == 1:
                for i, j in enumerate(GetObject.pouch):
                    if j == search_string:
                        GetObject.pouch.pop(i)
                for i, j in enumerate(GetObject.pouch):
                    if j == 'knotted \"The Earth Is Flat!\" sign':
                        GetObject.pouch.pop(i)
                for i, j in enumerate(GetObject.pouch):
                    if j == 'rope':
                        GetObject.pouch.pop(i)
                GetObject.pouch.append('knotted signs')
                # Set the status for knotted_signs to 1.
                GetObject.object_status['knotted_signs'] = 1

            # otherwise if the player hasn't knotted sign one yet, searches
            # for "Moon Landing" sign and removes it from inventory and adds
            # "knotted 'Moon Landing'" sign.
            else:
                for i, j in enumerate(GetObject.pouch):
                    if j == search_string:
                        GetObject.pouch.pop(i)
                GetObject.pouch.append('knotted \"The Moon Landing Was Faked!\" sign')

        # Swaps 'honey' for 'opened can of honey' in your inventory once the
        # player has opened it.
        elif search_string == 'can of honey':
            for i, j in enumerate(GetObject.pouch):
                if j == search_string:
                    GetObject.pouch.pop(i)
            GetObject.pouch.append('opened can of honey')
            # Set the status of can_opened to 1
            GetObject.object_status['can_opened'] = 1
            # Set the status of can_got to 0
            GetObject.object_status['can_got'] = 0

        # Removes the opened can of honey from the player's inventory if they
        # use it in room NINE.
        elif search_string == 'opened can of honey':
            for i, j in enumerate(GetObject.pouch):
                if j == search_string:
                    GetObject.pouch.pop(i)

        # Removes the knotted signs from the player's inventory after they
        # have made the pirate leave
        elif search_string == 'knotted signs':
            for i, j in enumerate(GetObject.pouch):
                if j == search_string:
                    GetObject.pouch.pop(i)

        else:
            print(">>>>> Logic Hole! Debug pouch_index_search()!")

    def using(self, room, words):
        '''
        Function for the player to use a single object or multiple objects
        in a the game. This function relies heavily on the
        UseObjectResponses.use_object_dict() dictionary in ex45_Responses.py.
        Read the comments for that dictionary to understand how it is
        constructed.

        UseObject.using() takes the parameters passed in to it in the form of
        the current room and a list of words constituting what is to be used.

        The list is reconstructed back into a string using
        UseObject.string_convert() and checked against the keys in
        UseObjectResponses.use_object_dict() dictionary. If it finds a
        match, the tuple values for that key are set to a variable. Those tuple
        values consist of a couple of object status variables from the
        GetObject.object_status() dictionary and a couple of functions from
        UseObjectResponses(). Each value from the tuple is assigned to its own
        variable and those variables are used for each valid scenario in the
        game when using an object or pair of objects.

        The UseObjectResponses.use_object_dict() dictionary has been restricted
        to four tuple values in order to maintain some semblence of a convention
        across all key pairs; vis a vis the positional order of each of the four
        tuple values, there are a few scenarios for UseObject.using() which
        require additional responses from UseObjectResponses(). In those cases
        the additional responses, beyond the two tuple responses in each key pair
        in UseObjectResponses.use_object_dict(), are called directly from the
        code (for example UseObjectResponses.do_not_have_item()). This is a bit
        of a kluge but I preferred this option to adding more responses to the
        tuple values.

        Some of the use cases for UseObject.using() involve changes to the
        nature of objects in the player's inventory. Since the players inventory
        is controlled by the GetObject.pouch() list, if a change to the inventory
        is needed by something that occurred in UseObject.using(),
        UseObject.pouch_search_index() will be called with a parameter of the
        object in GetObject.pouch() to be altered.

        Parameters:
            room (str): The current room the player is in.
            words (list): The remainder of the list of words from the original
                          player entered command. Contains all the remaining
                          words from the original player entered command once
                          "Get" has been popped off.
        '''
        # Reconstitute the words list back to a string and assign it to object.
        object = self.string_convert(self.words)
        # If there's no object
        if self.words == []:
            print("\nUse what? You didn't specify anything to use.")

        # Otherwise if object is a valid object key in
        # UsedObjectResponse.object_use_dict
        else:
            if object in UseObjectResponses.object_use_dict.keys():
                # Sets object_key to the tuple values for object in the dict.
                object_key = UseObjectResponses.object_use_dict[object]
                # Each value in the object_key tuple is broken out into a
                # separate variable
                object1_status = object_key[0]
                object2_status = object_key[1]
                function1_to_call = object_key[2]
                function2_to_call = object_key[3]
            # If object doesn't match a key in the object_use_dict it means
            # it wasn't typed correctly by the player.
            else:
                print("\nUse what? I didn't understand what you wrote.")

        # If the player tries to use the book anywhere.
        if object == '\"Learn Python The Hard Way\"':
            # If the player is in room FIVE and they have the book.
            if GetObject.object_status[object1_status] == 1 and self.room == 'five':
                # But if the pirate is already gone from the room the player
                # should not use the book.
                if GetObject.object_status[object2_status] == 1:
                    function2_to_call(self)
                # But if he's there, use the book.
                else:
                    function1_to_call(self)
            # If the player doesn't have the book.
            elif GetObject.object_status[object1_status] == 0:
                UseObjectResponses.do_not_have_item(self)
            # Otherwise if the player is in a room that doesn't need the book.
            else:
                function2_to_call(self)

        # If the player tries to use the mirror anywhere.
        if object == 'mirror':
            # If the player is in room ONE and has the mirror.
            if GetObject.object_status[object1_status] == 1 and self.room == 'one':
                # If Cthulhu has already been defeated
                if GetObject.object_status[object2_status] == 1:
                    function2_to_call(self)
                # If he hasn't been defeated yet.
                else:
                    function1_to_call(self)
                    # Set mirror_used to 1
                    GetObject.object_status[object2_status] = 1
            # If the player doesn't have the mirror.
            elif GetObject.object_status[object1_status] == 0:
                UseObjectResponses.do_not_have_item(self)
            # Otherwise if the player is in any other room with the mirror.
            else:
                function2_to_call(self)

        if object == 'knife':
            # If the player is in room FIVE
            if self.room == 'five':

                # If the pirate has already left the room.
                if GetObject.object_status[object2_status] == 1:
                    # And the player has a knife
                    if GetObject.object_status[object1_status] == 1:
                        UseObjectResponses.use_knife_nothing(self)
                    # Otherwise the player doesn't have a knife.
                    else:
                        function2_to_call(self)

                # Otherwise, if the pirate has not left the room.
                elif GetObject.object_status[object2_status] == 0:
                    # And the player has a knife
                    if GetObject.object_status[object1_status] == 1:
                        function1_to_call(self)
                    # Otherwise the player doesn't have a knife.
                    else:
                        function2_to_call(self)

            # Otherwise if the player is not in room FIVE.
            else:
                # And the player has a knife
                if GetObject.object_status[object1_status] == 1:
                    UseObjectResponses.use_knife_nothing(self)
                # Otherwise the player doesn't have a knife.
                else:
                    function2_to_call(self)

        if object == 'rope':
            # If the player has the rope
            if GetObject.object_status[object1_status] == 1:
                # If the signs have been tied already there's no rope left.
                if GetObject.object_status[object2_status] == 1:
                    function2_to_call(self)
                # If the player tries to use the rope by itself.
                else:
                    function1_to_call(self)
            # If the player doesn't have any rope to use.
            else:
                function2_to_call(self)

        if object == '\"The Earth Is Flat!\" sign':
            # If the player tries to use a sign by itself.
            if GetObject.object_status[object1_status] == 1:
                function1_to_call(self)
            # If the player doesn't have a sign but tries to use it.
            else:
                function2_to_call(self)

        if object == '\"The Moon Landing Was Faked!\" sign':
            # If the player tries to use a sign by itself.
            if GetObject.object_status[object1_status] == 1:
                function1_to_call(self)
            # If the player doesn't have a sign but tries to use it.
            else:
                function2_to_call(self)

        if object in ('rope with \"The Earth Is Flat!\" sign',
        '\"The Earth Is Flat!\" sign with rope'):
            # If the player already tied everything up.
            if GetObject.object_status['knotted_signs'] == 1:
                UseObjectResponses.all_knotted_up(self)
            else:
                # If the player already tied a knot around sign one.
                if GetObject.object_status['knotted_sign_one'] == 1:
                    function2_to_call(self)
                # If the player has the rope
                elif GetObject.object_status[object1_status] == 1:
                    # If the player has the sign.
                    if GetObject.object_status[object2_status] == 1:
                        function1_to_call(self)
                        # Set knotted_sign_one to 1
                        GetObject.object_status['knotted_sign_one'] = 1
                        # replace the sign in the pouch with a sign saying there
                        # is a rope tied around it.
                        self.pouch_index_search('\"The Earth Is Flat!\" sign')
                        # If this means both signs are tied in knots now.
                        if GetObject.object_status['knotted_signs'] == 1:
                            UseObjectResponses.tied_both_signs(self)

                    # If the player doesn't have the sign.
                    else:
                        print(dedent("""
                                     You don't have that sign!
                                     """))
                # If the player doesn't have the rope.
                else:
                    print(dedent("""
                                 You don't have any rope!
                                 """))

        if object in ('rope with \"The Moon Landing Was Faked!\" sign',
        '\"The Moon Landing Was Faked!\" sign with rope'):
            # If the player already tied everything up.
            if GetObject.object_status['knotted_signs'] == 1:
                UseObjectResponses.all_knotted_up(self)
            else:
                # If the player already tied a knot around sign two.
                if GetObject.object_status['knotted_sign_two'] == 1:
                    function2_to_call(self)
                # If the player has the rope
                elif GetObject.object_status[object1_status] == 1:
                    # If the player has the sign.
                    if GetObject.object_status[object2_status] == 1:
                        function1_to_call(self)
                        # Set knotted_sign_one to 1
                        GetObject.object_status['knotted_sign_two'] = 1
                        # replace the sign in the pouch with a sign saying there
                        # is a rope tied around it.
                        self.pouch_index_search('\"The Moon Landing Was Faked!\" sign')
                        # If this means both signs are tied in knots now.
                        if GetObject.object_status['knotted_signs'] == 1:
                            UseObjectResponses.tied_both_signs(self)

                    # If the player doesn't have the sign.
                    else:
                        print(dedent("""
                                     You don't have that sign!
                                     """))
                # If the player doesn't have the rope.
                else:
                    print(dedent("""
                                 You don't have any rope!
                                 """))

        # If the player tries to use the knotted signs
        if object == 'knotted signs':
            # If the player is in room FIVE
            if self.room == 'five':
                # If knotted_signs has been set to 3, indicating that they've
                # already been used up and removed from the player's inventory.
                if GetObject.object_status[object1_status] == 3:
                    UseObjectResponses.pirate_gone_with_signs(self)

                # Otherwise, if knotted_signs is set to 1, indicating that
                # they've been created, give them to the pirate.
                elif GetObject.object_status[object1_status] == 1:
                    function1_to_call(self)
                    # Set the status of knotted_signs to 3 to indicate they
                    # have been used up.
                    GetObject.object_status[object1_status] = 3
                    # Set the status of pirate_gone to 1
                    GetObject.object_status[object2_status] = 1
                    # Remove the knotted signs from the player's inventory
                    self.pouch_index_search('knotted signs')

                # Otherwise, the player doesn't have the knotted signs yet.
                else:
                    UseObjectResponses.do_not_have_item(self)

            # Otherwise, if the player is outside of room FIVE and
            # knotted_signs has been set to 3, indicating that they've been
            # used up.
            elif GetObject.object_status[object1_status] == 3:
                UseObjectResponses.pirate_gone_with_signs(self)

            # Otherwise the player is outside of room FIVE but hasn't used
            # the knotted signs up yet.
            else:
                function2_to_call(self)

        # if the player tries to use the unopened can of honey anywhere
        if object == 'can of honey':
            # If the can was already opened, the player should be blocked
            # from doing anything with an unopened can.
            if GetObject.object_status[object2_status] == 1:
                UseObjectResponses.do_not_have_item(self)
            else:
                # If the player has the can.
                if GetObject.object_status[object1_status] == 1:
                    # But it's not open.
                    if GetObject.object_status[object2_status] == 0:
                        function2_to_call(self)
                # Otherwise, if the player doesn't have the can of honey.
                else:
                    UseObjectResponses.do_not_have_item(self)

        # if the player tries to use the opened can of honey
        if object == 'opened can of honey':
            # If the player is in room NINE.
            if self.room == 'nine':
                # If the bear was already fed, don't do it again.
                if GetObject.object_status[object1_status] == 1:
                    UseObjectResponses.bear_already_fed(self)
                # If the can of honey has been opened, feed the bear.
                elif GetObject.object_status[object2_status] == 1:
                    function1_to_call(self)
                    GetObject.object_status['bear_eating'] = 1
                    self.pouch_index_search('opened can of honey')
                # Otherwise it's not open so don't use it.
                elif GetObject.object_status[object2_status] == 0:
                    function2_to_call(self)
            # If the bear is eating, the player has no more can of honey to use.
            elif GetObject.object_status[object1_status] == 1:
                UseObjectResponses.do_not_have_item(self)
            # There's nothing for the player to do with an open can outside
            # of room NINE.
            elif GetObject.object_status[object2_status] == 1:
                UseObjectResponses.no_use_for_item(self)
            # Otherwise the player doesn't have an open can of honey to use.
            else:
                UseObjectResponses.do_not_have_item(self)

        # if the player tries to use the knife with the unopened can of honey
        if object in ('knife with can of honey', 'can of honey with knife'):
            # If the can was already opened, the player should be blocked
            # from doing anything.
            if GetObject.object_status['can_opened'] == 1:
                UseObjectResponses.can_already_opened(self)
            else:
                # If the player has the knife.
                if GetObject.object_status[object1_status] == 1:
                    # But doesn't have the can of honey.
                    if GetObject.object_status[object2_status] == 0:
                        function2_to_call(self)
                    # Otherwise, cut away!
                    else:
                        function1_to_call(self)
                        # replace the unopened can of honey in the player's
                        # inventory with an opened can of honey.
                        self.pouch_index_search('can of honey')

                # Otherwise, if the player doesn't have the knife.
                else:
                    UseObjectResponses.no_got_knife(self)
